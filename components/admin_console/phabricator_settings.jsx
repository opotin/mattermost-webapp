// Copyright (c) 2015-present Mattermost, Inc. All Rights Reserved.
// See LICENSE.txt for license information.

import React from 'react';
import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';

import * as Utils from 'utils/utils.jsx';

import AdminSettings from './admin_settings.jsx';
import BooleanSetting from './boolean_setting.jsx';
import SettingsGroup from './settings_group.jsx';
import TextSetting from './text_setting.jsx';

export default class PhabricatorSettings extends AdminSettings {
    constructor(props) {
        super(props);

        this.getConfigFromState = this.getConfigFromState.bind(this);
        this.renderSettings = this.renderSettings.bind(this);
        this.updatePhabricatorUrl = this.updatePhabricatorUrl.bind(this);
    }

    getConfigFromState(config) {
        config.PhabricatorSettings.Enable = this.state.enable;
        config.PhabricatorSettings.Id = this.state.id;
        config.PhabricatorSettings.Secret = this.state.secret;
        config.PhabricatorSettings.UserApiEndpoint = this.state.userApiEndpoint;
        config.PhabricatorSettings.AuthEndpoint = this.state.authEndpoint;
        config.PhabricatorSettings.TokenEndpoint = this.state.tokenEndpoint;

        return config;
    }

    getStateFromConfig(config) {
        return {
            enable: config.PhabricatorSettings.Enable,
            id: config.PhabricatorSettings.Id,
            secret: config.PhabricatorSettings.Secret,
            phabricatorUrl: config.PhabricatorSettings.UserApiEndpoint.replace('/api/user.whoami', ''),
            userApiEndpoint: config.PhabricatorSettings.UserApiEndpoint,
            authEndpoint: config.PhabricatorSettings.AuthEndpoint,
            tokenEndpoint: config.PhabricatorSettings.TokenEndpoint,
        };
    }

    updatePhabricatorUrl(id, value) {
        let trimmedValue = value;
        if (value.endsWith('/')) {
            trimmedValue = value.slice(0, -1);
        }

        this.setState({
            saveNeeded: true,
            phabricatorUrl: value,
            userApiEndpoint: trimmedValue + '/api/user.whoami',
            authEndpoint: trimmedValue + '/oauthserver/auth/',
            tokenEndpoint: trimmedValue + '/oauthserver/token/',
        });
    }

    isPhabricatorURLSetByEnv = () => {
        // Assume that if one of these has been set using an environment variable,
        // all of them have been set that way
        return this.isSetByEnv('PhabricatorSettings.AuthEndpoint') ||
            this.isSetByEnv('PhabricatorSettings.TokenEndpoint') ||
            this.isSetByEnv('PhabricatorSettings.UserApiEndpoint');
    };

    renderTitle() {
        return (
            <FormattedMessage
                id='admin.authentication.phabricator'
                defaultMessage='Phabricator'
            />
        );
    }

    renderSettings() {
        return (
            <SettingsGroup>
                <BooleanSetting
                    id='enable'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.enableTitle'
                            defaultMessage='Enable authentication with Phabricator: '
                        />
                    }
                    helpText={
                        <div>
                            <FormattedMessage
                                id='admin.phabricator.enableDescription'
                                defaultMessage='When true, Mattermost allows team creation and account signup using Phabricator OAuth.'
                            />
                            <br/>
                            <FormattedHTMLMessage
                                id='admin.phabricator.EnableHtmlDesc'
                                defaultMessage='<ol><li>Log in to your Phabricator account and go to Applications -> OAuth Server -> Create OAuth Server.</li><li>Enter Redirect URIs "<your-mattermost-url>/login/phabricator/complete" (example: http://localhost:8065/login/phabricator/complete) and "<your-mattermost-url>/signup/phabricator/complete". </li><li>Then use "Show Application Secret" and "Client PHID" fields from Phabricator to complete the options below.</li><li>Complete the Endpoint URLs below. </li></ol>'
                            />
                        </div>
                    }
                    value={this.state.enable}
                    onChange={this.handleChange}
                    setByEnv={this.isSetByEnv('PhabricatorSettings.Enable')}
                />
                <TextSetting
                    id='id'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.clientIdTitle'
                            defaultMessage='Application ID:'
                        />
                    }
                    placeholder={Utils.localizeMessage('admin.phabricator.clientIdExample', 'E.g.: "jcuS8PuvcpGhpgHhlcpT1Mx42pnqMxQY"')}
                    helpText={
                        <FormattedMessage
                            id='admin.phabricator.clientIdDescription'
                            defaultMessage='Obtain this value via the instructions above for logging into Phabricator'
                        />
                    }
                    value={this.state.id}
                    onChange={this.handleChange}
                    disabled={!this.state.enable}
                    setByEnv={this.isSetByEnv('PhabricatorSettings.Id')}
                />
                <TextSetting
                    id='secret'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.clientSecretTitle'
                            defaultMessage='Application Secret Key:'
                        />
                    }
                    placeholder={Utils.localizeMessage('admin.phabricator.clientSecretExample', 'E.g.: "jcuS8PuvcpGhpgHhlcpT1Mx42pnqMxQY"')}
                    helpText={
                        <FormattedMessage
                            id='admin.phabricator.clientSecretDescription'
                            defaultMessage='Obtain this value via the instructions above for logging into Phabricator.'
                        />
                    }
                    value={this.state.secret}
                    onChange={this.handleChange}
                    disabled={!this.state.enable}
                    setByEnv={this.isSetByEnv('PhabricatorSettings.Secret')}
                />
                <TextSetting
                    id='phabricatorUrl'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.siteUrl'
                            defaultMessage='Phabricator Site URL:'
                        />
                    }
                    placeholder={Utils.localizeMessage('admin.phabricator.siteUrlExample', 'E.g.: https://')}
                    helpText={
                        <FormattedMessage
                            id='admin.phabricator.siteUrlDescription'
                            defaultMessage='Enter the URL of your Phabricator instance, e.g. https://example.com:3000. If your Phabricator instance is not set up with SSL, start the URL with http:// instead of https://.'
                        />
                    }
                    value={this.state.phabricatorUrl}
                    onChange={this.updatePhabricatorUrl}
                    disabled={!this.state.enable}
                    setByEnv={this.isPhabricatorURLSetByEnv()}
                />
                <TextSetting
                    id='userApiEndpoint'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.userTitle'
                            defaultMessage='User API Endpoint:'
                        />
                    }
                    placeholder={''}
                    value={this.state.userApiEndpoint}
                    disabled={true}
                    setByEnv={false}
                />
                <TextSetting
                    id='authEndpoint'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.authTitle'
                            defaultMessage='Auth Endpoint:'
                        />
                    }
                    placeholder={''}
                    value={this.state.authEndpoint}
                    disabled={true}
                    setByEnv={false}
                />
                <TextSetting
                    id='tokenEndpoint'
                    label={
                        <FormattedMessage
                            id='admin.phabricator.tokenTitle'
                            defaultMessage='Token Endpoint:'
                        />
                    }
                    placeholder={''}
                    value={this.state.tokenEndpoint}
                    disabled={true}
                    setByEnv={false}
                />
            </SettingsGroup>
        );
    }
}
